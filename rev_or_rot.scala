object RevRot {

  def revRot(strng: String, sz: Int): String = {
    
    def substr(s: String): String = {
      if (s.isEmpty || sz > s.length) "" 
      else if (s.map(x => x*x*x).sum % 2 == 0) s.reverse
      else (s.slice(1, s.length) + s(0))
    }
  
    if (sz == 0) ""
    else 
      strng
        .grouped(sz)
        .map(substr(_))
        .mkString("")
  }
}